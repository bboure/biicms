<?php
$root = dirname ( dirname ( dirname ( dirname ( __FILE__ ) ) ) );

require ($root . '/config/config.php');

return array (
		
		'basePath' => dirname ( __FILE__ ) . DIRECTORY_SEPARATOR . '..',
		'runtimePath' => $root . DIRECTORY_SEPARATOR . 'runtime',
	
		'import' => array (
				'application.components.*',
		),
		
		// application components
		'components' => array (

				'db' => array (
						'connectionString' => "mysql:host={$db_host};dbname={$db_name}",
						'username' => $db_username,
						'password' => $db_password,
						'charset' => 'utf8',
						'tablePrefix' => $db_prefix,
				),

				
				
				'log' => array (
						'class' => 'CLogRouter',
						'routes' => array (
								array (
										'class' => 'CFileLogRoute',
										'levels' => 'error, warning' 
								),
								array(
										'class'=>'CEmailLogRoute',
										'levels'=>'error, warning',
										'emails'=>array('benoit.boure@bbwebconsult.com'),
										'sentFrom' => 'benoit.boure@bbwebconsult.com',
										'enabled' => true
								),
						// uncomment the following to show log messages on web pages
						/*
						 * array( 'class'=>'CProfileLogRoute', ),
						 */
						)
						 
				),
				
				'authManager' => array(
				
						'class' => 'CDbAuthManager',
						'itemTable' => '{{auth_item}}',
						'itemChildTable' => '{{auth_item_child}}',
						'assignmentTable' => '{{auth_assignment}}',
				
				),
				
								
		),
		
);

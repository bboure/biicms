<?php
$root = dirname ( dirname ( dirname ( dirname ( __FILE__ ) ) ) );

require ($root . '/config/config.php');

if(!isset($adminPath))
	$adminPath = 'admin';

$modulesPath = $root . DIRECTORY_SEPARATOR . 'modules';

Yii::setPathOfAlias ( "modules", $modulesPath );
Yii::setPathOfAlias ( "runtime", $root . DIRECTORY_SEPARATOR . 'runtime' );
Yii::setPathOfAlias ( "medias", $root . DIRECTORY_SEPARATOR . 'medias' );

$config = array (
		
		'name' => isset ( $name ) ? $name : 'BiiCms',
		'theme' => $theme,
		'basePath' => dirname ( __FILE__ ) . DIRECTORY_SEPARATOR . '..',
		'runtimePath' => $root . DIRECTORY_SEPARATOR . 'runtime',
		
		//For maintenance
		'catchAllRequest' => !isset($maintenance) || $maintenance === false ? null : array('site/maintenance'), 
		
		// preloading 'log' component
		'preload' => array (
				'log',
				'languageManager' ,
				'moduleManager'
		),
		
		// autoloading model and component classes
		'import' => array (
				'application.models.*',
				'application.components.*',
				'application.components.widgets.*',
				'application.components.urlRules.*',
				'application.extensions.yii-mail.YiiMailMessage' 
		),
		
		'behaviors' => array (
				'ApplicationConfigBehavior' 
		),
		
		'modulePath' => $modulesPath,
		
		'modules' => CMap::mergeArray ( isset ( $modules ) && is_array ( $modules ) ? $modules : array (), array (
				'admin' => array (
						'class' => 'application.modules.admin.AdminModule' 
				) 
		) ),
		
		// application components
		'components' => array (
				
				'languageManager' => array (
						'class' => 'LanguageManager',
						'forceLangInUrl' => isset ( $forceLangInUrl ) ? $forceLangInUrl : true 
				),
				
				'user' => array (
						// enable cookie-based authentication
						'allowAutoLogin' => true 
				),
				
				// allows acces to user admin from front end
				'userAdmin' => array (
						'class' => 'AdminWebUser',
				),
				
				'db' => array (
						'connectionString' => "mysql:host={$db_host};dbname={$db_name}",
						'username' => $db_username,
						'password' => $db_password,
						'charset' => 'utf8',
						'tablePrefix' => $db_prefix,
						'enableProfiling' => YII_DEBUG,
						'enableParamLogging' => YII_DEBUG 
				),
				
				'urlManager' => array (
						'class' => 'UrlManager',
						'useStrictParsing' => true,
						'urlFormat' => 'path',
						'showScriptName' => false,
						'matchValue' => true,
						'urlSuffix' => isset($urlSuffix) ? $urlSuffix : null,
						'rules' => 

						CMap::mergeArray ( array (
								
								'mediaManager/<action:\w+>' => 'mediaManager/<action>',
								
								$adminPath => 'admin/site/index',
								$adminPath . '/<controller:\w+>' => 'admin/<controller>/index',
								$adminPath . '/<controller:\w+>/<action:\w+>' => 'admin/<controller>/<action>',
								$adminPath . '/<module:[\w\/]+>/<controller:\w+>/<action:\w+>' => 'admin/<module>/<controller>/<action>',
								
								'robots' => array (
										'site/robots',
										'pattern' => 'robots',
										'urlSuffix' => '.txt',
								),
								
								'sitemap' => array (
										'site/sitemap',
										'pattern' => 'sitemap',
										'urlSuffix' => '.xml',
								),
								
								'imageCache' => array(
										'class' => 'ImageCacheUrlRule',
								),
								
								'cmspage' => array(
										'class' => 'CmsPageUrlRule',
									),
								
								'blog' => array(
										'class' => 'BlogUrlRule',
								),
								
								'blogrss' => array(
										'class' => 'BlogRssUrlRule',
								),
								
								'blogpage' =>  array(
										'class' => 'BlogPageUrlRule',
								),
								
						)
						, isset ( $urlRules ) ? $urlRules : array () ) 
				),
				
				'moduleManager' => array( 'class' => 'ModuleManager' ),
				
				'themeManager' => array('themeClass' => 'Theme'),
				
				'mail' => array (
						'class' => 'ext.yii-mail.YiiMail' 
				),
				
				'errorHandler' => array (
						// use 'site/error' action to display errors
						'errorAction' => 'site/error' 
				),
				
				'log' => array (
						'class' => 'CLogRouter',
						'routes' => array (
								array (
										'class' => 'CFileLogRoute',
										'levels' => 'error, warning',
										'except' => 'exception.CHttpException.*', 
								),
								array(
										'class'=>'CEmailLogRoute',
										'levels'=>'error, warning',
										'except' => 'exception.CHttpException.*',
										'emails'=>array('benoit.boure@bbwebconsult.com'),
										'sentFrom' => 'benoit.boure@bbwebconsult.com',
										'enabled' => !YII_DEBUG
								),
						// uncomment the following to show log messages on web pages
						/*
						 * array( 'class'=>'CProfileLogRoute', ),
						 */
						)
						 
				),
				
				'messages' => array(

					'class' => 'PhpMessageSource',
						
				),
				
				'imageCache' =>
					
				array(
						'class' => 'ImageCacheComponent',
						'presets' => CMap::mergeArray(array(
							
									'msthumb' => array(
												'type' => 'outset',
												'width' => 100,
												'height' => 100
										),
								
								), 
									isset($imageCachePresets) ? $imageCachePresets : array() ),
				) ,
				
				'cache' => array(

									'class' => 'CFileCache' 
						
									),
									
				'clientScript' => array(
					
						'packages' => array(
								'jquery' => array(
												'js' => array('jquery.js'),
												'basePath' => 'application.assets.js',
								),
								
								'jquery.ui' => array(
												'js' => array('jquery-ui.js'),
												'depends'=>array('jquery'),
												'basePath' => 'application.assets.js',
								),
								'bootstrap-js' => array(
									'js' => array('bootstrap.min.js'),
									'depends'=>array('jquery'),
									'baseUrl' => '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/',
								),
								'bootstrap' => array(
										'css' => array('bootstrap.min.css', 'bootstrap-theme.min.css'),
										'depends'=>array('bootstrap-js'),
										'baseUrl' => '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/',
								),
								
							)
						
				),
				
				'authManager' => array(

							'class' => 'CDbAuthManager',
							'itemTable' => '{{auth_item}}',
							'itemChildTable' => '{{auth_item_child}}',
							'assignmentTable' => '{{auth_assignment}}',
						
						),
				
		),
		
		'params' => array (
				'enableUsers' => isset ( $enableUsers ) ? $enableUsers : false,
				'adminLangs' => array (
						'en' => 'English',
						'fr' => 'Français',
						'es' => 'Español' 
				),
				'adminEmail' => $admin_email,
				'adminPath' => $adminPath,
		) 
);

return isset( $customConfig ) && is_array( $customConfig ) ? CMap::mergeArray ( $config, $customConfig ) : $config;
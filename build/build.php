#!/usr/bin/env php
<?php

define('BII_PATH', dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."public_html");
define('BII_PROTECTED_PATH', dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR."public_html".DIRECTORY_SEPARATOR."core".DIRECTORY_SEPARATOR."protected");

$root=dirname(__FILE__);

// fix for fcgi
defined('STDIN') or define('STDIN', fopen('php://stdin', 'r'));

defined('YII_DEBUG') or define('YII_DEBUG',true);

require_once(dirname(dirname(__FILE__)).'/public_html/core/framework/yii.php');

Yii::setPathOfAlias('bii', BII_PROTECTED_PATH);

$config= array(
		'basePath'=>$root,
		'import' => array(
				'bii.models.*',
				'bii.components.*',
				'bii.components.widgets.*',
				'bii.components.urlRules.*',
				'bii.extensions.yii-mail.YiiMailMessage'
		),
		'components' => array(

				'authManager' => array(

						'class' => 'CDbAuthManager',
						'itemTable' => '{{auth_item}}',
						'itemChildTable' => '{{auth_item_child}}',
						'assignmentTable' => '{{auth_assignment}}',

				),

		),

);


$app=Yii::createConsoleApplication($config);
$app->commandRunner->addCommands(YII_PATH.'/cli/commands');

$env=@getenv('YII_CONSOLE_COMMANDS');
if(!empty($env))
	$app->commandRunner->addCommands($env);

$app->run();

<?php

class ParseCodeCommand extends CConsoleCommand
{
	
	private $_translations = array();
	private $_messages = array();
	
	private $_missingTranslations = array();
	private $_unusedTranslations = array();
	
	public function actionCheckTranslations($path = BII_PROTECTED_PATH)
	{
		
		$this->genReport($path);
		$this->printReport();
		
	}
	
	
	protected function getMessages($dir)
	{
		
		
		$handle = opendir($dir);
		
		while( ($file = readdir($handle)) != false)
		{
			if($file === '.' || $file === '..')
				continue;

			$filename = $dir . DIRECTORY_SEPARATOR . $file;
			
			
			
			if(is_file($filename) && substr($filename, -3) === 'php' )
			{
				$this->getMessagesInFile($filename);
			}
			else if(is_dir($filename))
			{
				$this->getMessages($filename);
			}
			
		}
				
	}
	
	
	protected function getMessagesInFile($file)
	{
		
		$content = file_get_contents($file);
		
		
		$handle = fopen($file, "r");
		if ($handle) {
			
			$n = 1;
			
			while (($line = fgets($handle)) !== false) {
				
				
				//$patern = '([\'"])([^\\3\\\\\\]*(?:\\\\.[^\\3\\\\\\]*)*)\\3';
				//search for anything that starts with ' or " and is followed by anything that is NOT ' or " (wichever was present)
				//optionaly, it can be folloed by \ followed by the \\3 (' or ") and anuthing that is NOT ' or " . This last part can be present several times
				$patern = "(['\"])((?:(?!\\3)[^\\\\])*(\\\\\\3(?:(?!\\3)[^\\\\])*)*)\\3";
				preg_match_all("/Yii::t\s*\(\s*(['\"])([\w\.]+)\\1\s*,\s*".$patern."/s", $line, $matches);
								
				// $matches[1] Category
				// $matches[2] String
				
				if(count($matches[2])>0)
				{
					for($i = 0; $i<count($matches[2]) ; $i++)
					{
						$category = $matches[2][$i];
						$string = 	$matches[4][$i];
						
						if($category === 'yii')
							continue;
						
						if(strpos($string, "\\".$matches[3][$i]) !== false)
						{
							//remove the escaping backslash
							$string = str_replace("\\".$matches[3][$i], $matches[3][$i], $string);
						}
						
						$this->_messages[$category][$string] = array ( $string, $file, $n ) ;
					}
							
				}
				
				$n++;
				
			}
		
		}
		
		
	}
	
		
	private function getTranslations($path)
	{
		
		$dir = $path . DIRECTORY_SEPARATOR . "messages";
		
		if(is_dir($dir))
		{
		
		$handle = opendir($dir);
		while( ($file = readdir($handle)) !== false)
		{
			
			if($file === '.' || $file === '..')
				continue;
			
			if(is_dir($dir . DIRECTORY_SEPARATOR . $file))
				$this->getTranslationsLang($path, $file);
			
		}

		}
		
	}
	
	private function getTranslationsLang($path, $lang)
	{
		
		$dir = $path . DIRECTORY_SEPARATOR . "messages" . DIRECTORY_SEPARATOR . $lang;
		
		$handle = opendir($dir);
		while( ($file = readdir($handle)) !== false)
		{
				
			if($file === '.' || $file === '..')
				continue;
				
			$filename = $dir . DIRECTORY_SEPARATOR . $file;
		
			$category = substr($file, 0, strlen($file)-4);
			
			if(strpos($path,'modules'))
			{
				$category = ucfirst(basename($path)).'Module.'.$category;
			}
			
			if(is_file($filename))
			{
				$this->_translations[$lang][$category] = require $filename;
			}
			
				
		}
		
	}
	
	protected function genReport($path)
	{
		
		$this->getMessages($path . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'protected');
		$this->getMessages($path . DIRECTORY_SEPARATOR . 'modules');
		$this->getTranslations($path . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'protected');
		
		$handle = opendir($path . DIRECTORY_SEPARATOR . 'modules');
		while( ($file = readdir($handle)) !== false)
		{
			
			if($file === '.' || $file === '..')
				continue;
			
			if(is_dir($path . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $file))
				$this->getTranslations($path . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . $file);
			
		}
		
		
		//missing
		foreach($this->_messages as $category => $messages)
		{
			
			foreach($messages as $message)
			{
				
				foreach($this->_translations  as $lang => $categories)
				{
					
					if( !isset( $categories[$category][$message[0]] )  )
					{
						
						if(!isset($this->_missingTranslations[$lang][$category][$message[0]]))
							$this->_missingTranslations[$lang][$category][$message[0]] = array();
						
						$this->_missingTranslations[$lang][$category][$message[0]][] = substr($message[1], strlen($path)) . " at line " . $message[2];
					}
				}
			}
			
			
		}
		
		
		//unused
		
		foreach($this->_translations  as $lang => $categories)
		{
				
			foreach($categories as $category => $translations)
			{
				
				foreach($translations as $message => $translation)
				{

					
										
					if(!isset( $this->_messages[$category][$message] ) )
					{
												
						if(!isset($this->_unusedTranslations[$lang][$category]))
							$this->_unusedTranslations[$lang][$category] = array();
						
						$this->_unusedTranslations[$lang][$category][] = $message;
					}

				}
				
			}
			
		}
		
		
	}
	
	protected function printReport()
	{
		
		echo "<h1>Missing Translations</h1>";
		
		if(count($this->_missingTranslations))
		{
		
			foreach($this->_missingTranslations as $lang => $categories)
			{
	
				echo "Report for $lang\n";
				echo CHtml::tag("table", array( 'border' => 1, "style" => 'border: 1px; width:100%;'));	
				
				foreach($categories as $category => $strings)
				{
								
					echo CHtml::tag("tr");
					echo CHtml::tag("td", array("colspan" => 2, "style" => 'color: #fff; background-color:#1e1e1e;'));
						echo $category;
					echo CHtml::closeTag("tr");
					echo CHtml::closeTag("td");
				
					foreach($strings as $string => $files)
					{
						
						echo CHtml::tag("tr");
							echo CHtml::tag("td",array(),htmlentities($string),true);
							echo CHtml::tag("td", array(), implode("<br/>", $files), true);
						echo CHtml::closeTag("tr");
					}
					
				}
				
				echo CHtml::closeTag("table");
				
			}
		
		}
		else
		{
			echo "There is no missing translation<br/>";	
		}
		
		
		echo "<h1>Unused Translations</h1>";
		
		if(count($this->_unusedTranslations ) > 0)
		{
					
			foreach($this->_unusedTranslations as $lang => $categories)
			{
				
				
				echo "Report for $lang\n";
				echo CHtml::tag("table", array( 'border' => 1, "style" => 'border: 1px; width:100%;'));
					
				foreach($categories as $category => $strings)
				{
						
					echo CHtml::tag("tr");
					echo CHtml::tag("td", array("style" => 'color: #fff; background-color:#1e1e1e;'));
					echo $category;
					echo CHtml::closeTag("tr");
					echo CHtml::closeTag("td");
						
					foreach($strings as $string)
					{
							
						echo CHtml::tag("tr");
						echo CHtml::tag("td",array(),htmlentities($string),true);
						echo CHtml::closeTag("tr");
					}
				
				}
					
				echo CHtml::closeTag("table");
				
		}
		
		}
		else
		{
			echo "There is no unused translation<br/>";
		}
		
		
	}
	
	
	
}
<?php

class ReleaseCommand extends CConsoleCommand
{
	
	public $zipNoIgnoreFiles = array('.htaccess', 'index.php', 'index.html');
	
	public $zipIgnore = array(
		
			'assets/*',
			'config/*',
			'medias/cache/*',
			'medias/docs/*',
			'medias/files/*',
			'medias/medias/*',
			'medias/images/*',
			'medias/users/*',
			'modules/*',
			'runtime/*',
			'users/*',
			'themes/*',
			
	);
	
	
	public function actionRelease($v)
	{
		$this->actionBumpVersion($v);
		$this->actionZip("BiiCMS-{$v}.zip");
	}
	
	public function actionBumpVersion($v)
	{
		
		$version = $v;
		
		$content = file_get_contents(BII_PROTECTED_PATH . DIRECTORY_SEPARATOR . "components" . DIRECTORY_SEPARATOR . "ApplicationConfigBehavior.php");
		//update version
		$content = preg_replace("/(public function getVersion.*?return \")(.*?)(\")/ms", '${1}'.$version.'${3}', $content);
		file_put_contents(BII_PROTECTED_PATH . DIRECTORY_SEPARATOR . "components" . DIRECTORY_SEPARATOR . "ApplicationConfigBehavior.php", $content);
		
		if(substr($v, -4) === '-dev' )
			$mode = 'dev';
		else 
			$mode = 'rc';
		
		$this->actionMode($mode);
		
	}
	
	public function actionMode($mode)
	{
		
		$debug = $mode === 'dev' ? 'true' : 'false';
		
		echo "changing YII_DEBUG to " . $debug;
		
		$content = file_get_contents(BII_PATH . DIRECTORY_SEPARATOR . "index.php");
		//update version
		$content = preg_replace("/define\('YII_DEBUG',(true|false)\)/", "define('YII_DEBUG',$debug)", $content);
		
		file_put_contents(BII_PATH . DIRECTORY_SEPARATOR . "index.php", $content);
		
	}
	
	public function actionZip($filename = 'BiiCMS.zip', $force = false)
	{
		
		$destination = dirname(Yii::app()->basePath) . DIRECTORY_SEPARATOR . "releases" . DIRECTORY_SEPARATOR . $filename;
		
		if(!$force && file_exists($destination))
		{
			echo "The file already exists!";
			Yii::app()->end();
		}
		elseif($force && file_exists($destination))
		{
			@unlink($destination);
		}
		
		$zipFile = new ZipArchive();
		$zipFile->open($destination, ZipArchive::CREATE);
		$this->folderToZip(BII_PATH, $zipFile);
		
		$zipFile->close();
		
		echo "$destination has beed created";
		
	}
	
	private function folderToZip($folder, &$zipFile, $subfolder = null) {

			if ($zipFile == null) {
		        // no resource given, exit
		        return false;
		    }
		    // we check if $folder has a slash at its end, if not, we append one
		    $folder .= end(str_split($folder)) == "/" ? "" : "/";
		    
		    if($subfolder === null)
		    	$subfolder = '';
		    else
		    	$subfolder .= end(str_split($subfolder)) == "/" ? "" : "/";
		    
		    // we start by going through all files in $folder
		    $handle = opendir($folder);
		    while ($f = readdir($handle)) {
		        if ( in_array($f, $this->zipNoIgnoreFiles) || ( $f[0] != "." && !in_array($subfolder.$f, $this->zipIgnore) && !in_array($subfolder."*", $this->zipIgnore)) ) {
		        	
		            if (is_file($folder . $f)  && !in_array($subfolder."{f}", $this->zipIgnore) ) {
		                // if we find a file, store it
		                // if we have a subfolder, store it there
		                 $zipFile->addFile($folder . $f, $subfolder . $f);
		                
		            } elseif (is_dir($folder . $f) && !in_array($subfolder."{d}", $this->zipIgnore)) {
		                // if we find a folder, create a folder in the zip 
		               $zipFile->addEmptyDir($subfolder.$f);
		                // and call the function again
	               		$this->folderToZip($folder . $f, $zipFile, $subfolder.$f);
		            }
		        }
		    }
		}
	
}
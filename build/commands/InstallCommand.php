<?php


class InstallCommand extends CConsoleCommand
{
	
	
	public function actionIndex($path)
	{
		$path = rtrim($path,'/');
		require_once $path . '/core/protected/components/Migration.php';
		require_once $path . '/core/protected/components/CoreMigration.php'; 
		
		$app_name = $this->prompt('Application name:','');
		
		$db_host = $this->prompt('Database host:', '127.0.0.1');
		$db_name = $this->prompt('Database name:');
		$db_username = $this->prompt('Database username:');
		$db_password = $this->prompt('Database password:');
		
		$db_prefix = $this->prompt('Database prefix:', '');
		
		$lang = $this->prompt('Language:', 'es');
		
		$admin_email = $this->prompt('Admin Email:');
		$admin_password = $this->prompt('Admin Password:');
		
		$db = new CDbConnection("mysql:host={$db_host};dbname={$db_name};", $db_username, $db_password);
		$db->tablePrefix = $db_prefix;
		$db->charset = 'utf8';
		$db->setActive(true);
		
		Yii::app()->setComponent('db', $db);
		
		preg_match('/public function getVersion.*?return \"([^\"]*)\"/ms', file_get_contents($path . "/core/protected/components/ApplicationConfigBehavior.php"), $matches);
		
		$version = $matches[1];
		
		$migration = new CoreMigration();
		$migration->installDb();
		$migration->insertMigrationRecord($version);
		
		$db->createCommand()->insert("{{admin}}", array("name" => "Admin", "email" => $admin_email, "password" => md5($admin_password)));

		Yii::app()->authManager->assign('Superadmin', $admin_email);
		
		$db->createCommand()->insert("{{lang}}", array("name" => 'Español', "code" => 'es', "default" => $lang === 'es', 'active' => $lang === 'es'  ) );
		$db->createCommand()->insert("{{lang}}", array("name" => 'English', "code" => 'en', "default" => $lang === 'en', 'active' => $lang === 'en'  ) );
		$db->createCommand()->insert("{{lang}}", array("name" => 'Français', "code" => 'fr', "default" => $lang === 'fr', 'active' => $lang === 'fr'  ) );
		
		
		//create basic config
		$config = <<<config
<?php
	/* Database */
	
	\$db_host = "$db_host"; //host of the database
	\$db_name = "$db_name"; //name of the database
	\$db_username = "$db_username"; //username
	\$db_password = "$db_password"; //password
	\$db_prefix = "$db_prefix";
	
	/* Config */
	
	\$name = "$app_name"; //name of the website
	\$theme = null; //theme of the website (null = no theme)
	\$admin_email = "$admin_email";
	\$forceLangInUrl = true;
	
	\$modules = array();
	
	\$imageCachePresets = array();

	\$customConfig = array();
			
	//url rules
	\$urlRules = array();
			
config;
		
		
	file_put_contents($path . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'config.php', $config);
		
	}
	
	public function actionUpdate($host, $database, $username, $password, $db_prefix)
	{
			
		$db = new CDbConnection("mysql:host={$host};dbname={$database};", $username, $password);
		$db->tablePrefix = $db_prefix;
		$db->charset = 'utf8';
		$db->setActive(true);
		
		Yii::app()->setComponent('db', $db);
		
		$coreMigration = new CoreMigration();
		$coreMigration->updateDb();
		
	}
	
}